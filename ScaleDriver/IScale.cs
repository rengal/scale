﻿namespace ScaleDriver
{
    interface IScale
    {
        void Connect(int portNumber, int baudRate);
        void Connect(string ip, int port);
        void Disconnect();
        WeightingResult GetWeight(int timeStable);
    }
}
