﻿using System;
using NUnit.Framework;

namespace ScaleDriver
{
    [TestFixture]
    public sealed class DriverTests
    {
        private readonly ScaleDriver driver = new ScaleDriver();

        /// <summary>
        /// Имя порта не может быть отрицательным
        /// </summary>
        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void ConnectInvalidPort()
        {
            driver.Connect(-1, 0);
        }
    }

}