﻿using System;
using NUnit.Framework;

namespace ScaleDriver
{
    [TestFixture]
    public sealed class SerializerTests
    {
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DeserializeNullArgument()
        {
            // ReSharper disable AssignNullToNotNullAttribute
            ScaleSerializer.Deserialize((string)null);
            // ReSharper restore AssignNullToNotNullAttribute
        }

        
        /// <summary>
        /// Частично пришедший, но правильный ответ от устройства не является ошибкой. В этом случае нужно возвращать null
        /// </summary>
        [Test]
        public void DeserializeIncompleteResponse1()
        {
            var result = ScaleSerializer.Deserialize(string.Empty); 
            Assert.IsNull(result);
        }

        /// <summary>
        /// Частично пришедший, но правильный ответ от устройства не является ошибкой. В этом случае нужно возвращать null
        /// </summary>
        [Test]
        public void DeserializeIncompleteResponse2()
        {
            var result = ScaleSerializer.Deserialize(string.Empty); //заменить string.Empty на какой-нибудь частично пришедший ответ от устройства
            Assert.IsNull(result);
        }

        /// <summary>
        /// В случае некорректного ответа дожна возвращаться ошибка с кодом IncorrectResponse
        /// </summary>
        [Test]
        public void DeserializeIncorrectResponse()
        {
            try
            {
                ScaleSerializer.Deserialize("bla-bla-bla");
            }
            catch (ScaleException e)
            {
                Assert.AreEqual(e.CommunicationResultCode, CommunicationResultCode.IncorrectResponse);
            }
        }

        /// <summary>
        /// Пример успешного парсинга от устройства
        /// </summary>
        [Test]
        public void DeserializeOk1()
        {
            var result = ScaleSerializer.Deserialize("тут нужно заменить на некоторые полезные данных");
            Assert.AreEqual(WeightingState.Stable, result.State);
            Assert.AreEqual(12.345, result.Weight);
            Assert.AreEqual(Unitweight.Kilogramm, result.Unit);
        }

        /// <summary>
        /// Другой пример успешного парсинга от устройства (сделать несколько тестов с разными тестовыми данными)
        /// </summary>
        [Test]
        public void DeserializeOk2()
        {
            var result = ScaleSerializer.Deserialize("тут нужно заменить на некоторые полезные данных");
            Assert.AreEqual(WeightingState.Unstable, result.State);
            Assert.AreEqual(4321, result.Weight);
            Assert.AreEqual(Unitweight.Gramm, result.Unit);
        }
    }
}
