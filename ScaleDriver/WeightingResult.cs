﻿namespace ScaleDriver
{
    /// <summary>
    /// Единица измерения веса
    /// </summary>
    public enum Unitweight
    {
        Gramm = 1,
        Kilogramm = 2,
        Ounce = 3,
        Pound = 4
    }

    /// <summary>
    /// Состояние весов при взвешивании
    /// </summary>
    public enum WeightingState
    {
        /// <summary>
        /// Неопределённое (весы не включены, перевес или т.п. ошибка)
        /// </summary>
        Unverfified = 0,

        /// <summary>
        /// Стабильное
        /// </summary>
        Stable = 1,

        /// <summary>
        /// Нестабильное
        /// </summary>
        Unstable = 2
    }

    public class WeightingResult
    {
        public readonly double Weight;
        public readonly WeightingState State;
        public readonly Unitweight Unit;

        public WeightingResult(double weight, WeightingState state, Unitweight unit)
        {
            Weight = weight;
            State = state;
            Unit = unit;
        }
    }
}
