﻿using System;
using ScaleDriver;

namespace ScaleDriver
{
    internal enum CommunicationResultCode
    {
        NoError = 0,
        PortOpenError = 1,
        IncorrectResponse = 2,
        OperationTimeout=3
    }
}
    class ScaleException:Exception
    {
        public readonly CommunicationResultCode CommunicationResultCode;

        public ScaleException(CommunicationResultCode code)
        {
            CommunicationResultCode = code;
        }
    }
