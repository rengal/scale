﻿using System;
using System.IO.Ports;
using System.Text;
using System.Threading;

namespace ScaleDriver
{
    public class ScaleDriver : IScale
    {
        private SerialPort _serialPort;

        public void Connect(int portNumber, int baudRate)
        {
            try
            {
                _serialPort = new SerialPort("COM" + portNumber.ToString(), baudRate);
                _serialPort.Open();
            }
            catch 
            {
                throw new ScaleException(CommunicationResultCode.PortOpenError);
            }
            
        }

        public void Connect(string ip, int port)
        {
            throw new NotImplementedException();
        }

        public void Disconnect()
        {
            try
            {
                _serialPort.Close();
                _serialPort.Dispose();
            }
            catch { }
        }

        public WeightingResult GetWeight(int timeStable)
        {
            
            byte[] scaleResponse = new byte[16];

            if (_serialPort == null || !_serialPort.IsOpen)
                throw new ScaleException(CommunicationResultCode.IncorrectResponse);
                        
            _serialPort.DiscardInBuffer(); // очищаем буффер

            _serialPort.Write(new byte[] {0x57, 0x0D }, 0, 2); // команда получить вес
            Thread.Sleep(100);
            _serialPort.ReadTimeout = timeStable;
            try
            {
                _serialPort.Read(scaleResponse, 0, 16); // считываем ответ
            }
            catch
            {
                throw new ScaleException(CommunicationResultCode.OperationTimeout);
            }

            return ScaleSerializer.Deserialize(scaleResponse);
        }
    }
}