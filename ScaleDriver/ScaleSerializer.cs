﻿using System;
using System.Text;

namespace ScaleDriver
{
    public class ScaleSerializer
    {
        const byte unitWeightLength = 2;
        const byte unitWeightOffsset = 7;
            const byte dotAsDelimiterOffsset = 3;

        public static WeightingResult Deserialize(byte[] data)
        {
            double _weight = 0.00;
            WeightingState _state = WeightingState.Unverfified;
            Unitweight _unit = Unitweight.Kilogramm;

            // смотрим, если в 4-м байте точка, то вес получен, иначе вес не стабилен
            if (data[dotAsDelimiterOffsset] == 0x2E)
            {
                // смотрим, если весы вернули не KG, то считаем, что ответ неопределенный т.к. не знаем в какой единице измерения возвращаемый вес
                // если такое случится, то значит весы прошиты не для нашего региона где метрическая система и нужно будет смотреть какие символы еденицы изменения возвращают весы 
                // и доработать драйвер, т.к. не известно какие символы будут например для региона Америка или Англия
                if (Encoding.ASCII.GetString(data, unitWeightOffsset, unitWeightLength) == "KG")
                {
                    try
                    {
                        // согласно протоколу NCI cash register protocol со 2-го по 7-й байт находится вес
                        _weight = Convert.ToDouble(Encoding.ASCII.GetString(data, 1, 6), System.Globalization.CultureInfo.InvariantCulture);
                        _state = WeightingState.Stable;
                    }
                    catch // если в результате конвертации в double ошибка, значит в ответе неизвестно что и нет четкого веса
                    {
                        _state = WeightingState.Unverfified;
                    }
                }
            }
            else
                _state = WeightingState.Unstable;

            return new WeightingResult(_weight, _state, _unit);
        }

        public static WeightingResult Deserialize(string data)
        {
            throw new NotImplementedException();
        }
    }
}
